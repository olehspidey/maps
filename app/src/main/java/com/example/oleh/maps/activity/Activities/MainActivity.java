package com.example.oleh.maps.activity.Activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.view.menu.MenuView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessException;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.files.BackendlessFile;
import com.bumptech.glide.Glide;
import com.example.oleh.maps.R;
import com.example.oleh.maps.activity.Constants.Constants;
import com.example.oleh.maps.activity.Models.ImageModel;
import com.example.oleh.maps.activity.Requests.ImageRequest;
import com.example.oleh.maps.activity.Models.PeopleModel;
import com.example.oleh.maps.activity.Requests.PlaceModel;
import com.example.oleh.maps.activity.Requests.Request;
import com.example.oleh.maps.activity.Requests.RequestValue;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback {
    private GoogleMap mMap;
    private List<PlaceModel.ItemPlace> placeModel = new ArrayList<>();
    private List<PeopleModel.ItemPeople> peopleModels = new ArrayList<>();
    private List<ImageModel.ItemImage> imageList = new ArrayList<>();
    private ImageView
            av;
    private TextView
            nameOfPeople,
            emailOfPeople;
    private final int
            RESULT_PIC_FROM_CAMERA = 1,
            RESULT_PIC_ROM_GALARY = 2;
    private ImageView
            imageView;
    private String
            getLon,
            getLat,
            getCityName,
            email,
            title,
            time,
            path,
            pNameCam,
            objectId;
    private Double
            getDobLon,
            getDobLat;

    private MenuView.ItemView
            iMap,
            iPeople,
            iPlaces,
            iLogOut;
    private Button
            searchCityBut;
    private EditText
            searcCityEdit;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainn);
        setTitle("Maps");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        initUI();
        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View hed = navigationView.getHeaderView(0);
        final TextView na = (TextView) hed.findViewById(R.id.nameOfPeople);
        TextView em = (TextView) hed.findViewById(R.id.emailOfPeople);
        av = (ImageView) hed.findViewById(R.id.imageView);
        av.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this, R.style.AppTheme_AlertDialog)
                        .setTitle("Do you want to choose avatar?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                AlertDialog.Builder builder2 = new AlertDialog.Builder(MainActivity.this, R.style.AppTheme_AlertDialog)
                                        .setTitle("Choose image")
                                        .setPositiveButton("Choose image from camera", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                Intent intentCam = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                                Date date = new Date(System.currentTimeMillis());
                                                time = String.valueOf(date.getTime());
                                                File file = new File(Environment.getExternalStorageDirectory(), time + ".png");
                                                Uri photodir = Uri.fromFile(file);
                                                path = photodir.toString();
                                                Uri photoPath = Uri.parse(path);
                                                intentCam.putExtra(MediaStore.EXTRA_OUTPUT, path);
                                                startActivityForResult(intentCam, RESULT_PIC_FROM_CAMERA);


                                            }
                                        })
                                        .setNegativeButton("Choose image from gallary", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                Intent intentGal = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                                startActivityForResult(intentGal, RESULT_PIC_ROM_GALARY);
                                            }
                                        });
                                builder2.show();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        });
                builder.show();


                return false;
            }
        });

        em.setText(email);

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Request service = retrofit.create(Request.class);
        final Call<PeopleModel> user = service.getUserByAppId(Constants.API_APPLICATION_ID, Constants.API_SECRET_KEY);
        user.enqueue(new Callback<PeopleModel>() {
                         @Override
                         public void onResponse(Call<PeopleModel> call, Response<PeopleModel> response) {
                             PeopleModel people = response.body();
                             peopleModels = people.getData();
                             for (int i = 0; i < peopleModels.size(); i++) {
                                 if (peopleModels.get(i).getEmail().equals(email)) {
                                     try {
                                         na.setText(peopleModels.get(i).getName().toString());

                                     } catch (Exception e) {
                                         Toast.makeText(getApplicationContext(), "Warning:loaded wrong name, please refreash application!", Toast.LENGTH_SHORT).show();
                                     }

                                 }
                             }
                             for (int i = 0; i < peopleModels.size(); i++) {
                                 if (peopleModels.get(i).getEmail().equals(email)) {
                                     objectId = peopleModels.get(i).getObjectId().toString();
                                 }
                                 break;
                             }
                             final Retrofit retrofit = new Retrofit.Builder()
                                     .baseUrl(Constants.BASE_URL)
                                     .addConverterFactory(GsonConverterFactory.create())
                                     .build();
                             Request service = retrofit.create(Request.class);
                             final Call<ImageModel> image = service.getImage(Constants.API_APPLICATION_ID, Constants.API_SECRET_KEY);
                             image.enqueue(new Callback<ImageModel>() {
                                               @Override
                                               public void onResponse(Call<ImageModel> call, Response<ImageModel> response) {
                                                   ImageModel imageModel = response.body();
                                                   imageList = imageModel.getData();
                                                   int i;
//                                                           try {
//                                                           Toast.makeText(getApplicationContext(),imageList.size()+"",Toast.LENGTH_SHORT).show();
//                                                           Toast.makeText(getApplicationContext(),objectId+"",Toast.LENGTH_SHORT).show();
//                                                           Toast.makeText(getApplicationContext(),imageList.get(0).getConnectedPlace()+"",Toast.LENGTH_SHORT).show();
//                                                           Toast.makeText(getApplicationContext(),imageList.get(1).getConnectedPlace()+"",Toast.LENGTH_SHORT).show();
//                                                           Toast.makeText(getApplicationContext(),imageList.get(2).getConnectedPlace()+"",Toast.LENGTH_SHORT).show();
//                                                           Toast.makeText(getApplicationContext(),imageList.get(3).getConnectedPlace()+"",Toast.LENGTH_SHORT).show();
//                                                           Toast.makeText(getApplicationContext(),imageList.get(4).getConnectedPlace()+"",Toast.LENGTH_SHORT).show();
//                                                           Toast.makeText(getApplicationContext(),imageList.get(5).getConnectedPlace()+"",Toast.LENGTH_SHORT).show();
//                                                           Toast.makeText(getApplicationContext(),imageList.get(6).getConnectedPlace()+"",Toast.LENGTH_SHORT).show();
//                                                           Toast.makeText(getApplicationContext(),imageList.get(7).getConnectedPlace()+"",Toast.LENGTH_SHORT).show();
//                                                           Toast.makeText(getApplicationContext(),imageList.get(8).getConnectedPlace()+"",Toast.LENGTH_SHORT).show();
//                                                           Toast.makeText(getApplicationContext(),imageList.get(9).getConnectedPlace()+"",Toast.LENGTH_SHORT).show();
//                                                           Toast.makeText(getApplicationContext(),imageList.get(10).getConnectedPlace()+"",Toast.LENGTH_SHORT).show();
//                                                           Toast.makeText(getApplicationContext(), imageList.get(0).getConnectedPlace().toString(), Toast.LENGTH_LONG).show();
//                                                           Toast.makeText(getApplicationContext(), objectId, Toast.LENGTH_LONG).show();

                                                   for (i = 0; i < imageList.size(); i++) {
                                                       if (imageList.get(i).getConnectedPlace().equals(objectId)) {
//                                                                   Toast.makeText(getApplicationContext(), objectId, Toast.LENGTH_LONG).show();
                                                           try {
                                                               Glide
                                                                       .with(getApplicationContext())
                                                                       .load(Constants.BASE_URL + Constants.API_APPLICATION_ID + "/v1/files/" + imageList.get(i).getFullPath() + "/" + imageList.get(i).getOwnerId())
                                                                       .placeholder(R.drawable.loading)
                                                                       .into(av);
                                                           } catch (Exception e) {

                                                               Toast.makeText(getApplicationContext(), "False data image", Toast.LENGTH_LONG).show();
                                                           }
                                                       }
                                                       break;
                                                   }
//                                                           } catch (Exception e) {
//                                                               Toast.makeText(getApplicationContext(), "False data image", Toast.LENGTH_LONG).show();
//
//                                                           }
                                               }

                                               @Override
                                               public void onFailure(Call<ImageModel> call, Throwable t) {
                                                   Toast.makeText(getApplicationContext(), "No data of image, pleas refreash application", Toast.LENGTH_LONG).show();
                                               }
                                           }

                             );

//                                 }
//                             }

                         }

                         @Override
                         public void onFailure(Call<PeopleModel> call, Throwable t) {
                             Toast.makeText(getApplicationContext(), "Warning:Name don't load,please refreash application!", Toast.LENGTH_SHORT).show();
                         }
                     }

        );


        navigationView.setNavigationItemSelectedListener(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        if (getLat != null && getLon != null && getCityName != null) {
            getDobLat = Double.valueOf(getLat);
            getDobLon = Double.valueOf(getLon);
        }


    }

    private void initUI() {
        iMap = (MenuView.ItemView) findViewById(R.id.iMap);
        iPeople = (MenuView.ItemView) findViewById(R.id.iPeople);
        iPlaces = (MenuView.ItemView) findViewById(R.id.iPlaces);
        iLogOut = (MenuView.ItemView) findViewById(R.id.iLogOut);
        searcCityEdit = (EditText) findViewById(R.id.searcCityEditt);
        searchCityBut = (Button) findViewById(R.id.searchCityButt);
        nameOfPeople = (TextView) findViewById(R.id.nameOfPeople);
        emailOfPeople = (TextView) findViewById(R.id.emailOfPeople);
        imageView = (ImageView) findViewById(R.id.imageView);
        searchCityBut = (Button) findViewById(R.id.searchCityButt);
        searcCityEdit = (EditText) findViewById(R.id.searcCityEditt);
        getLat = getIntent().getStringExtra("Lat");
        getLon = getIntent().getStringExtra("Lon");
        getCityName = getIntent().getStringExtra("CityName");
        email = getIntent().getStringExtra("email");
        title = getIntent().getStringExtra("title");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case RESULT_PIC_FROM_CAMERA:
                if (resultCode == RESULT_OK) {
                    final Bitmap photo = (Bitmap) data.getExtras().get("data");
                    Toast.makeText(getApplicationContext(), photo.toString(), Toast.LENGTH_SHORT).show();
                    Backendless.Files.Android.upload(photo,
                            Bitmap.CompressFormat.PNG,
                            100,
                            pNameCam = photo.hashCode() + ".png",
                            path,
//
                            new AsyncCallback<BackendlessFile>() {
                                @Override
                                public void handleResponse(final BackendlessFile backendlessFile) {
                                    Glide
                                            .with(MainActivity.this)
                                            .load("https://api.backendless.com/892B27F2-C4C2-42E0-FF52-5EAD8C9A4400/v1/files/" + path.toString() + "/" + pNameCam)
                                            .placeholder(R.drawable.loading)
                                            .error(R.mipmap.ic_launcher)
                                            .skipMemoryCache(false)
                                            .into(av);
                                    Toast.makeText(getApplicationContext(), "Photo loaded success!", Toast.LENGTH_SHORT).show();

//
                                }

                                //
                                @Override
                                public void handleFault(BackendlessFault backendlessFault) {
                                    Toast.makeText(MainActivity.this, backendlessFault.toString(), Toast.LENGTH_SHORT).show();
                                }
                            });
//
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(Constants.BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    Request service = retrofit.create(Request.class);
                    ImageRequest request = new ImageRequest(
                            objectId, path, pNameCam);
                    Log.e("TAG", new Gson().toJson(request));
                    Call<ImageModel> userInfo = service.createImage(Constants.API_APPLICATION_ID, Constants.API_SECRET_KEY, request);
                    userInfo.enqueue(new Callback<ImageModel>() {

                        @Override
                        public void onResponse(Call<ImageModel> call, Response<ImageModel> response) {
                            Log.e("TAG", String.valueOf(response.raw()));
                            if (response.isSuccessful()) {
                            }
                        }

                        @Override
                        public void onFailure(Call<ImageModel> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), "Fault", Toast.LENGTH_SHORT).show();
                        }
                    });

                }
                break;
            case RESULT_PIC_ROM_GALARY:
                final String photoPath = getFirstImage(data);
                Bitmap photo = BitmapFactory.decodeFile(photoPath);
                Toast.makeText(getApplicationContext(), photoPath, Toast.LENGTH_SHORT).show();
                Toast.makeText(getApplicationContext(), photo.toString(), Toast.LENGTH_SHORT).show();
                Backendless.Files.Android.upload(photo,
                        Bitmap.CompressFormat.PNG,
                        100,
                        pNameCam = photo.hashCode() + ".png",
                        photoPath,

                        new AsyncCallback<BackendlessFile>() {
                            @Override
                            public void handleResponse(final BackendlessFile backendlessFile) {
                                Glide
                                        .with(MainActivity.this)
                                        .load("https://api.backendless.com/892B27F2-C4C2-42E0-FF52-5EAD8C9A4400/v1/files/" + photoPath + "/" + pNameCam)
                                        .placeholder(R.drawable.loading)
                                        .error(R.mipmap.ic_launcher)
                                        .skipMemoryCache(false)
                                        .into(av);
                                Toast.makeText(getApplicationContext(), "Photo loaded success!", Toast.LENGTH_SHORT).show();

                            }

                            @Override
                            public void handleFault(BackendlessFault backendlessFault) {
                                Toast.makeText(MainActivity.this, backendlessFault.toString(), Toast.LENGTH_SHORT).show();
                            }
                        });
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(Constants.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                Request service = retrofit.create(Request.class);
                ImageRequest request = new ImageRequest(
                        objectId, photoPath, pNameCam);
                Log.e("TAG", new Gson().toJson(request));
                Call<ImageModel> userInfo = service.createImage(Constants.API_APPLICATION_ID, Constants.API_SECRET_KEY, request);
                userInfo.enqueue(new Callback<ImageModel>() {

                    @Override
                    public void onResponse(Call<ImageModel> call, Response<ImageModel> response) {
                        Log.e("TAG", String.valueOf(response.raw()));
                        if (response.isSuccessful()) {
                        }
                    }

                    @Override
                    public void onFailure(Call<ImageModel> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "Fault", Toast.LENGTH_SHORT).show();
                    }
                });

                break;
        }

    }

    private String getFirstImage(Intent data) {
        Uri selectedImage = data.getData();
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(selectedImage,
                filePathColumn, null, null, null);

        if (cursor == null || cursor.getCount() < 1) {
            return null; // no cursor or no record. DO YOUR ERROR HANDLING
        }

        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        if (columnIndex < 0) // no column index
            return null; // DO YOUR ERROR HANDLING

        String picturePath = cursor.getString(columnIndex);

        cursor.close(); // close cursor
        return picturePath;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.mainn, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
//        FragmentManager fm = getFragmentManager();
        int id = item.getItemId();
        if (id == R.id.iPeople) {
            Intent intent = new Intent(MainActivity.this, PeopleActivity.class);
            startActivity(intent);
        }
        if (id == R.id.iLogOut) {
            BackendlessUser user;
            try {
                // now log out:
                Backendless.UserService.logout();
            } catch (BackendlessException exception) {
                // logout failed, to get the error code, call exception.getFault().getCode()
                Intent intent = new Intent(MainActivity.this, FirstActivity.class);
                finish();
                startActivity(intent);
            }
        }
        if (id == R.id.iMap) {
            Intent intent = new Intent(MainActivity.this, MapsActivity.class);
            startActivity(intent);
        }
        if (id == R.id.iPlaces) {
            Intent intent = new Intent(MainActivity.this, PlacesActivity.class);
            startActivity(intent);
        }
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (getLat == null && getLon == null && getCityName == null) {

            final Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            Request service = retrofit.create(Request.class);
            final Call<PlaceModel> user = service.getPlaceByAddIp(Constants.API_APPLICATION_ID, Constants.API_SECRET_KEY);
            user.enqueue(new Callback<PlaceModel>() {
                @Override
                public void onResponse(Call<PlaceModel> call, Response<PlaceModel> response) {
                    PlaceModel people = response.body();
                    placeModel = people.getData();
                    for (int i = 0; i < placeModel.size(); i++) {
                        LatLng city = new LatLng(Double.valueOf(placeModel.get(i).getLatitude()), Double.valueOf(placeModel.get(i).getLongitude()));
                        mMap.addMarker(new MarkerOptions()
                                .position(city)
                                .title(placeModel.get(i).getName())
                        );
                    }
                }

                @Override
                public void onFailure(Call<PlaceModel> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Fault", Toast.LENGTH_SHORT).show();
                }
            });
            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(final Marker marker) {

                    final Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(Constants.BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    Request service = retrofit.create(Request.class);
                    final Call<PlaceModel> user = service.getPlaceByAddIp(Constants.API_APPLICATION_ID, Constants.API_SECRET_KEY);
                    user.enqueue(new Callback<PlaceModel>() {
                        @Override
                        public void onResponse(Call<PlaceModel> call, Response<PlaceModel> response) {
                            PlaceModel people = response.body();
                            placeModel = people.getData();
                            for (int i = 0; i < placeModel.size(); i++) {
                                if (placeModel.get(i).getName().equals(marker.getTitle())) {
                                    Intent intent = new Intent(MainActivity.this, ShowPhotosActivity.class);
                                    intent.putExtra("idPl", placeModel.get(i).getObjectId());
                                    startActivity(intent);
                                }
                            }

                        }

                        @Override
                        public void onFailure(Call<PlaceModel> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), "Fault", Toast.LENGTH_SHORT).show();
                        }
                    });


                    return false;
                }
            });

            searchCityBut.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(Constants.BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    Request service = retrofit.create(Request.class);
                    Call<RequestValue> weather = service.getWeatherByName(searcCityEdit.getText().toString(), Constants.API_APPLICATION_ID_LATLON);
                    weather.enqueue(new Callback<RequestValue>() {
                        @Override
                        public void onResponse(Call<RequestValue> call, Response<RequestValue> response) {
                            RequestValue cityCoords = response.body();
                            LatLng latLng = new LatLng(cityCoords.getCoord().getLat(), cityCoords.getCoord().getLon());
                            mMap.addMarker(new MarkerOptions().position(latLng).title(searcCityEdit.getText().toString()));
                            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                        }

                        @Override
                        public void onFailure(Call<RequestValue> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), t.getLocalizedMessage(), Toast.LENGTH_SHORT);
                        }
                    });
                }
            });
            mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                @Override
                public void onMapLongClick(LatLng latLng) {
                    LatLng city = new LatLng(latLng.latitude, latLng.longitude);
                    mMap.addMarker(new MarkerOptions().position(city).title(title));
                    Intent intent = new Intent(MainActivity.this, NewPlacesActivity.class);
                    intent.putExtra("lat", latLng.latitude);
                    intent.putExtra("long", latLng.longitude);
                    startActivity(intent);
                }
            });

        } else {


            LatLng city = new LatLng(getDobLat, getDobLon);

            mMap.addMarker(new MarkerOptions()
                    .position(city)
                    .title(getCityName)
            );
            mMap.moveCamera(CameraUpdateFactory.newLatLng(city));
            mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                @Override
                public void onMapLongClick(LatLng latLng) {
                    LatLng city = new LatLng(latLng.latitude, latLng.longitude);
                    mMap.addMarker(new MarkerOptions().position(city).title(title));
                    Intent intent = new Intent(MainActivity.this, NewPlacesActivity.class);
                    intent.putExtra("lat", latLng.latitude);
                    intent.putExtra("long", latLng.longitude);
                    startActivity(intent);
                }
            });

            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(final Marker marker) {

                    final Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(Constants.BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    Request service = retrofit.create(Request.class);
                    final Call<PlaceModel> user = service.getPlaceByAddIp(Constants.API_APPLICATION_ID, Constants.API_SECRET_KEY);
                    user.enqueue(new Callback<PlaceModel>() {
                        @Override
                        public void onResponse(Call<PlaceModel> call, Response<PlaceModel> response) {
                            PlaceModel people = response.body();
                            placeModel = people.getData();
                            for (int i = 0; i < placeModel.size(); i++) {
                                if (placeModel.get(i).getName().equals(marker.getTitle())) {
                                    Intent intent = new Intent(MainActivity.this, ShowPhotosActivity.class);
                                    intent.putExtra("idPl", placeModel.get(i).getObjectId());
                                    startActivity(intent);
                                }
                            }

                        }

                        @Override
                        public void onFailure(Call<PlaceModel> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), "Fault", Toast.LENGTH_SHORT).show();
                        }
                    });


                    return false;
                }
            });
        }
    }

}
