package com.example.oleh.maps.activity.Activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.oleh.maps.R;
import com.example.oleh.maps.activity.Adapters.AdapterPlaces;
import com.example.oleh.maps.activity.Constants.Constants;
import com.example.oleh.maps.activity.Requests.PlaceModel;
import com.example.oleh.maps.activity.Requests.Request;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PlacesActivity extends AppCompatActivity {
    private String objectId;
    private List<PlaceModel.ItemPlace> placeModel = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_places);
        setTitle("Places");
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        final AdapterPlaces adapter = new AdapterPlaces(placeModel);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Request service = retrofit.create(Request.class);
        final Call<PlaceModel> user = service.getPlaceByAddIp(Constants.API_APPLICATION_ID, Constants.API_SECRET_KEY);
        user.enqueue(new Callback<PlaceModel>() {
            @Override
            public void onResponse(Call<PlaceModel> call, Response<PlaceModel> response) {
                PlaceModel people = response.body();
                placeModel = people.getData();
                adapter.setList(placeModel);
                adapter.notifyDataSetChanged();
                adapter.setOnClickListener(new AdapterPlaces.OnClickListener() {
                    @Override
                    public void onClick(final int position) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(PlacesActivity.this, R.style.AppTheme_AlertDialog)
                                .setTitle("Show photo on map or fullscreen?")
                                .setPositiveButton("Show photo on map", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Intent intent = new Intent(PlacesActivity.this, MapsActivity.class);
                                        intent.putExtra("CityName", placeModel.get(position).getName());
                                        intent.putExtra("Lat", placeModel.get(position).getLatitude().toString());
                                        intent.putExtra("Lon", placeModel.get(position).getLongitude().toString());
                                        startActivity(intent);
//                                        Toast.makeText(getApplicationContext(),placeModel.get(position).getLatitude()+" "+ placeModel.get(position).getLongitude()+" ",Toast.LENGTH_LONG).show();
                                    }
                                })
                                .setNegativeButton("Show photo fullscreen", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Intent intent = new Intent(PlacesActivity.this, ShowPhotosActivity.class);
                                        intent.putExtra("idPl", placeModel.get(position).getObjectId());
                                        startActivity(intent);
                                    }
                                });
                        builder.show();
                    }

                    @Override
                    public void onLongClick(int position) {
                        objectId = placeModel.get(position).getObjectId();
                        Intent intent = new Intent(PlacesActivity.this, ImageLoadActivity.class);
                        intent.putExtra("OID", objectId);
                        startActivity(intent);
                    }
                });

            }

            @Override
            public void onFailure(Call<PlaceModel> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fault", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
