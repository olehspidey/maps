package com.example.oleh.maps.activity.Activities;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.oleh.maps.R;
import com.example.oleh.maps.activity.Constants.Constants;
import com.example.oleh.maps.activity.Requests.PlaceModel;
import com.example.oleh.maps.activity.Requests.Request;
import com.example.oleh.maps.activity.Requests.RequestValue;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    private List<PlaceModel.ItemPlace> placeModel = new ArrayList<>();
    private String
            getLon,
            getLat,
            title;
    private Double
            getDobLon,
            getDobLat;
    private String getCityName;
    private GoogleMap mMap;
    private Button
            searchCityBut;
    private EditText
            searcCityEdit;
    private TextView nameOfPeople;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        initUI();
        NavigationView navigationView = (NavigationView) findViewById(R.id.drawer_layout);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        if (getLat != null && getLon != null && getCityName != null) {
            getDobLat = Double.valueOf(getLat);
            getDobLon = Double.valueOf(getLon);
        }

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (getLat == null && getLon == null && getCityName == null) {

            final Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            Request service = retrofit.create(Request.class);
            final Call<PlaceModel> user = service.getPlaceByAddIp(Constants.API_APPLICATION_ID, Constants.API_SECRET_KEY);
            user.enqueue(new Callback<PlaceModel>() {
                @Override
                public void onResponse(Call<PlaceModel> call, Response<PlaceModel> response) {
                    PlaceModel people = response.body();
                    placeModel = people.getData();
                    for (int i = 0; i < placeModel.size(); i++) {
                        LatLng city = new LatLng(Double.valueOf(placeModel.get(i).getLatitude()), Double.valueOf(placeModel.get(i).getLongitude()));
                        mMap.addMarker(new MarkerOptions()
                                .position(city)
                                .title(placeModel.get(i).getName())
                        );
                    }
                }

                @Override
                public void onFailure(Call<PlaceModel> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Fault", Toast.LENGTH_SHORT).show();
                }
            });
            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(final Marker marker) {

                    final Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(Constants.BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    Request service = retrofit.create(Request.class);
                    final Call<PlaceModel> user = service.getPlaceByAddIp(Constants.API_APPLICATION_ID, Constants.API_SECRET_KEY);
                    user.enqueue(new Callback<PlaceModel>() {
                        @Override
                        public void onResponse(Call<PlaceModel> call, Response<PlaceModel> response) {
                            PlaceModel people = response.body();
                            placeModel = people.getData();
                            for (int i = 0; i < placeModel.size(); i++) {
                                if (placeModel.get(i).getName().equals(marker.getTitle())) {
                                    Intent intent = new Intent(MapsActivity.this, ShowPhotosActivity.class);
                                    intent.putExtra("idPl", placeModel.get(i).getObjectId());
                                    startActivity(intent);
                                }
                            }

                        }

                        @Override
                        public void onFailure(Call<PlaceModel> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), "Fault", Toast.LENGTH_SHORT).show();
                        }
                    });


                    return false;
                }
            });

            searchCityBut.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(Constants.BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    Request service = retrofit.create(Request.class);
                    Call<RequestValue> weather = service.getWeatherByName(searcCityEdit.getText().toString(), Constants.API_APPLICATION_ID_LATLON);
                    weather.enqueue(new Callback<RequestValue>() {
                        @Override
                        public void onResponse(Call<RequestValue> call, Response<RequestValue> response) {
                            RequestValue cityCoords = response.body();
                            LatLng latLng = new LatLng(cityCoords.getCoord().getLat(), cityCoords.getCoord().getLon());
                            mMap.addMarker(new MarkerOptions().position(latLng).title(searcCityEdit.getText().toString()));
                            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                        }

                        @Override
                        public void onFailure(Call<RequestValue> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), t.getLocalizedMessage(), Toast.LENGTH_SHORT);
                        }
                    });
                }
            });
            mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                @Override
                public void onMapLongClick(LatLng latLng) {
                    LatLng city = new LatLng(latLng.latitude, latLng.longitude);
                    mMap.addMarker(new MarkerOptions().position(city).title(title));
                    Intent intent = new Intent(MapsActivity.this, NewPlacesActivity.class);
                    intent.putExtra("lat", latLng.latitude);
                    intent.putExtra("long", latLng.longitude);
                    startActivity(intent);
                }
            });

        } else {


            LatLng city = new LatLng(getDobLat, getDobLon);

            mMap.addMarker(new MarkerOptions()
                    .position(city)
                    .title(getCityName)
            );
            mMap.moveCamera(CameraUpdateFactory.newLatLng(city));
            mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                @Override
                public void onMapLongClick(LatLng latLng) {
                    LatLng city = new LatLng(latLng.latitude, latLng.longitude);
                    mMap.addMarker(new MarkerOptions().position(city).title(title));
                    Intent intent = new Intent(MapsActivity.this, NewPlacesActivity.class);
                    intent.putExtra("lat", latLng.latitude);
                    intent.putExtra("long", latLng.longitude);
                    startActivity(intent);
                }
            });

            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(final Marker marker) {

                    final Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(Constants.BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    Request service = retrofit.create(Request.class);
                    final Call<PlaceModel> user = service.getPlaceByAddIp(Constants.API_APPLICATION_ID, Constants.API_SECRET_KEY);
                    user.enqueue(new Callback<PlaceModel>() {
                        @Override
                        public void onResponse(Call<PlaceModel> call, Response<PlaceModel> response) {
                            PlaceModel people = response.body();
                            placeModel = people.getData();
                            for (int i = 0; i < placeModel.size(); i++) {
                                if (placeModel.get(i).getName().equals(marker.getTitle())) {
                                    Intent intent = new Intent(MapsActivity.this, ShowPhotosActivity.class);
                                    intent.putExtra("idPl", placeModel.get(i).getObjectId());
                                    startActivity(intent);
                                }
                            }

                        }

                        @Override
                        public void onFailure(Call<PlaceModel> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), "Fault", Toast.LENGTH_SHORT).show();
                        }
                    });


                    return false;
                }
            });
        }

    }

    private void initUI() {
        searchCityBut = (Button) findViewById(R.id.searchCityBut);
        searcCityEdit = (EditText) findViewById(R.id.searcCityEdit);
        getLat = getIntent().getStringExtra("Lat");
        getLon = getIntent().getStringExtra("Lon");
        getCityName = getIntent().getStringExtra("CityName");
        title = getIntent().getStringExtra("title");
        nameOfPeople = (TextView) findViewById(R.id.nameOfPeople);

    }
}
