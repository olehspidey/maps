package com.example.oleh.maps.activity.Activities;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.files.BackendlessFile;
import com.bumptech.glide.Glide;
import com.example.oleh.maps.R;
import com.example.oleh.maps.activity.Constants.Constants;
import com.example.oleh.maps.activity.Models.ImageModel;
import com.example.oleh.maps.activity.Requests.ImageRequest;
import com.example.oleh.maps.activity.Requests.Request;
import com.google.gson.Gson;

import java.io.File;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ImageLoadActivity extends AppCompatActivity implements View.OnClickListener {
    private String
            objectId,
            pNameCam,
            pNameGal,
            time,
            path;
    private final int
            RESULT_PIC_FROM_CAMERA = 1,
            RESULT_PIC_ROM_GALARY = 2;
    private Button
            bLoadImageGal,
            bLoadImageCam;
    private ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_load);
        objectId = getIntent().getStringExtra("OID");
        initUI();
        setListeners();
    }


    private void initUI() {
        bLoadImageGal = (Button) findViewById(R.id.bLoadImageGal);
        bLoadImageCam = (Button) findViewById(R.id.bLoadImageCam);
        img = (ImageView) findViewById(R.id.Img);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case RESULT_PIC_FROM_CAMERA:
                if (resultCode == RESULT_OK) {
                    final Bitmap photo = (Bitmap) data.getExtras().get("data");
                    Toast.makeText(getApplicationContext(), photo.toString(), Toast.LENGTH_SHORT).show();
                    Backendless.Files.Android.upload(photo,
                            Bitmap.CompressFormat.PNG,
                            100,
                            pNameCam = photo.hashCode() + ".png",
                            path,
//
                            new AsyncCallback<BackendlessFile>() {
                                @Override
                                public void handleResponse(final BackendlessFile backendlessFile) {
                                    Glide
                                            .with(ImageLoadActivity.this)
//                                            .load(photo)
                                            .load("https://api.backendless.com/892B27F2-C4C2-42E0-FF52-5EAD8C9A4400/v1/files/" + path.toString() + "/" + pNameCam)
                                            .placeholder(R.drawable.loading)
                                            .error(R.mipmap.ic_launcher)
                                            .skipMemoryCache(false)
                                            .into(img);
                                    Toast.makeText(getApplicationContext(), "Photo loaded success!", Toast.LENGTH_SHORT).show();

//
                                }

                                //
                                @Override
                                public void handleFault(BackendlessFault backendlessFault) {
                                    Toast.makeText(ImageLoadActivity.this, backendlessFault.toString(), Toast.LENGTH_SHORT).show();
                                }
                            });
//
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl("http://api.backendless.com/")
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    Request service = retrofit.create(Request.class);
                    ImageRequest request = new ImageRequest(
                            objectId, path, pNameCam);
                    Log.e("TAG", new Gson().toJson(request));
                    Call<ImageModel> userInfo = service.createImage(Constants.API_APPLICATION_ID, Constants.API_SECRET_KEY, request);
                    userInfo.enqueue(new Callback<ImageModel>() {

                        @Override
                        public void onResponse(Call<ImageModel> call, Response<ImageModel> response) {
                            Log.e("TAG", String.valueOf(response.raw()));
                            if (response.isSuccessful()) {
                            }
                        }

                        @Override
                        public void onFailure(Call<ImageModel> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), "Fault", Toast.LENGTH_SHORT).show();
                        }
                    });

                }
                break;
            case RESULT_PIC_ROM_GALARY:
                final String photoPath = getFirstImage(data);
                Bitmap photo = BitmapFactory.decodeFile(photoPath);
                Toast.makeText(getApplicationContext(), photoPath, Toast.LENGTH_SHORT).show();
                Toast.makeText(getApplicationContext(), photo.toString(), Toast.LENGTH_SHORT).show();
                Backendless.Files.Android.upload(photo,
                        Bitmap.CompressFormat.PNG,
                        100,
                        pNameCam = photo.hashCode() + ".png",
                        photoPath,

                        new AsyncCallback<BackendlessFile>() {
                            @Override
                            public void handleResponse(final BackendlessFile backendlessFile) {
                                Glide
                                        .with(ImageLoadActivity.this)
                                        .load("https://api.backendless.com/892B27F2-C4C2-42E0-FF52-5EAD8C9A4400/v1/files/" + photoPath + "/" + pNameCam)
                                        .placeholder(R.drawable.loading)
                                        .error(R.mipmap.ic_launcher)
                                        .skipMemoryCache(false)
                                        .into(img);
                                Toast.makeText(getApplicationContext(), "Photo loaded success!", Toast.LENGTH_SHORT).show();

                            }

                            @Override
                            public void handleFault(BackendlessFault backendlessFault) {
                                Toast.makeText(ImageLoadActivity.this, backendlessFault.toString(), Toast.LENGTH_SHORT).show();
                            }
                        });
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(Constants.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                Request service = retrofit.create(Request.class);
                ImageRequest request = new ImageRequest(
                        objectId, photoPath, pNameCam);
                Log.e("TAG", new Gson().toJson(request));
                Call<ImageModel> userInfo = service.createImage(Constants.API_APPLICATION_ID, Constants.API_SECRET_KEY, request);
                userInfo.enqueue(new Callback<ImageModel>() {

                    @Override
                    public void onResponse(Call<ImageModel> call, Response<ImageModel> response) {
                        Log.e("TAG", String.valueOf(response.raw()));
                        if (response.isSuccessful()) {
                        }
                    }

                    @Override
                    public void onFailure(Call<ImageModel> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "Fault", Toast.LENGTH_SHORT).show();
                    }
                });

                break;
        }

    }


    private String getFirstImage(Intent data) {
        Uri selectedImage = data.getData();
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(selectedImage,
                filePathColumn, null, null, null);

        if (cursor == null || cursor.getCount() < 1) {
            return null; // no cursor or no record. DO YOUR ERROR HANDLING
        }

        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        if (columnIndex < 0) // no column index
            return null; // DO YOUR ERROR HANDLING

        String picturePath = cursor.getString(columnIndex);

        cursor.close(); // close cursor
        return picturePath;
    }

    private void setListeners() {
        bLoadImageCam.setOnClickListener(this);
        bLoadImageGal.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bLoadImageCam:
                Intent intentCam = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                Date date = new Date(System.currentTimeMillis());
                time = String.valueOf(date.getTime());
                File file = new File(Environment.getExternalStorageDirectory(), time + ".png");
                Uri photodir = Uri.fromFile(file);
                path = photodir.toString();
                Uri photoPath = Uri.parse(path);
                intentCam.putExtra(MediaStore.EXTRA_OUTPUT, path);
                startActivityForResult(intentCam, RESULT_PIC_FROM_CAMERA);
                break;
            case R.id.bLoadImageGal:
                Intent intentGal = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intentGal, RESULT_PIC_ROM_GALARY);
                break;
        }
    }
}
